/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugo.callcenter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugo.business.OperatorHandler;
import org.apache.commons.text.RandomStringGenerator;
import org.hugo.business.DirectorHandler;
import org.hugo.business.MachineHandler;
import org.hugo.business.SupervisorHandler;
import org.hugo.common.Director;
import org.hugo.common.Machine;
import org.hugo.common.Operator;
import org.hugo.common.Supervisor;

/**
 * Application entrance point
 * @author Hugo Armando Castellanos Morales
 */
public class CallCenter {
    /**
     * Applications main function
     * @param args Program arguments
     */
    public static void main(String[] args){
        OperatorHandler operatorHandler=new OperatorHandler();
        for(int i=0;i<4;i++){
            operatorHandler.registerEmployee(new Operator(Integer.toString(i),new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(15)));
        }
        
        SupervisorHandler supervisorHandler=new SupervisorHandler();
        for(int j=0;j<2;j++){
            supervisorHandler.registerEmployee(new Supervisor(Integer.toString(j+4),new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(15)));
        }
        DirectorHandler directorHandler=new DirectorHandler();
        directorHandler.registerEmployee(new Director("7", new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(15)));
        
        MachineHandler machineHandler=new MachineHandler();
        for(int i=0;i<4;i++){
            machineHandler.registerEmployee(new Machine(Integer.toString(i),new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(15)));
        }
        
        operatorHandler.setNextServer(supervisorHandler);
        supervisorHandler.setNextServer(directorHandler);
        directorHandler.setNextServer(machineHandler);
        machineHandler.setNextServer(operatorHandler);
        
        org.hugo.business.Dispatcher mainDispatcher=new org.hugo.business.Dispatcher();
        mainDispatcher.setFirstHandler(operatorHandler);
        
        ExecutorService executorService=Executors.newFixedThreadPool(10);
        for(int i=0;i<100;i++){
            System.out.println("Incomming call "+(i+1));
            executorService.submit(() -> {
                mainDispatcher.dispatchCall();
                return true;
            });
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(CallCenter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        executorService.shutdown();
        try {
            if(executorService.awaitTermination(1, TimeUnit.HOURS))
            {
                System.out.println("Termination done");
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(CallCenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
