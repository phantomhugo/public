/*
 * This file was written by Hugo Armando Castellanos Morales
 */
package org.hugo.test;

import org.hugo.business.DirectorHandler;
import org.hugo.business.MachineHandler;
import org.hugo.business.OperatorHandler;
import org.hugo.business.SupervisorHandler;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo Armando Castellanos Morales
 */
public class EmptyHandlerTest {
    private OperatorHandler operatorHandler;
    private SupervisorHandler supervisorHandler;
    private DirectorHandler directorHandler;
    private MachineHandler machineHandler;
    public EmptyHandlerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        operatorHandler=new OperatorHandler();
        supervisorHandler=new SupervisorHandler();
        directorHandler=new DirectorHandler();
        machineHandler=new MachineHandler();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void EmptyOperatorTest(){
        assertEquals(false, operatorHandler.serve());
    }
    
    @Test
    public void EmptySupervisorTest(){
        assertEquals(false,supervisorHandler.serve());
    }
    
    @Test
    public void EmptyDirectorTest(){
        assertEquals(false,directorHandler.serve());
    }
    @Test
    public void EmptyMachineTest(){
        assertEquals(false,machineHandler.serve());
    }
}
