/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugo.test;

import org.apache.commons.text.RandomStringGenerator;
import org.hugo.business.DirectorHandler;
import org.hugo.business.MachineHandler;
import org.hugo.business.OperatorHandler;
import org.hugo.business.SupervisorHandler;
import org.hugo.common.Director;
import org.hugo.common.Machine;
import org.hugo.common.Operator;
import org.hugo.common.Supervisor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugo
 */
public class SingleHandlerTest {
    private OperatorHandler operatorHandler;
    private SupervisorHandler supervisorHandler;
    private DirectorHandler directorHandler;
    private MachineHandler machineHandler;
    public SingleHandlerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        operatorHandler=new OperatorHandler();
        supervisorHandler=new SupervisorHandler();
        directorHandler=new DirectorHandler();
        machineHandler=new MachineHandler();
        operatorHandler.registerEmployee(new Operator("1",new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(5)));
        supervisorHandler.registerEmployee(new Supervisor("2",new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(5)));
        directorHandler.registerEmployee(new Director("3", new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(5)));
        machineHandler.registerEmployee(new Machine("4",new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(5)));
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void SingleOperatorTest(){
        operatorHandler.serve();
        assertEquals(false, operatorHandler.serve());
    }
    
    @Test
    public void SingleSupervisorTest(){
        supervisorHandler.serve();
        assertEquals(false,supervisorHandler.serve());
    }
    
    @Test
    public void SingleDirectorTest(){
        directorHandler.serve();
        assertEquals(false,directorHandler.serve());
    }
    @Test
    public void SingleMachineTest(){
        machineHandler.serve();
        assertEquals(false,machineHandler.serve());
    }
}
