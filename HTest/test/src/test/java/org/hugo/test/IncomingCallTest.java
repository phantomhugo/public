/*
 * This file was written by Hugo Armando Castellanos Morales
 */
package org.hugo.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.text.RandomStringGenerator;
import org.hugo.business.DirectorHandler;
import org.hugo.business.MachineHandler;
import org.hugo.business.OperatorHandler;
import org.hugo.business.SupervisorHandler;
import org.hugo.common.Director;
import org.hugo.common.Machine;
import org.hugo.common.Operator;
import org.hugo.common.Supervisor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hugo
 */
public class IncomingCallTest {
    private OperatorHandler operatorHandler;
    private SupervisorHandler supervisorHandler;
    private DirectorHandler directorHandler;
    private MachineHandler machineHandler;
    private org.hugo.business.Dispatcher mainDispatcher;
    private ExecutorService executorService;
    public IncomingCallTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        operatorHandler=new OperatorHandler();
        supervisorHandler=new SupervisorHandler();
        directorHandler=new DirectorHandler();
        machineHandler=new MachineHandler();
        mainDispatcher=new org.hugo.business.Dispatcher();
        for(int i=0;i<4;i++){
            operatorHandler.registerEmployee(new Operator(Integer.toString(i),new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(5)));
        }        
        for(int j=0;j<2;j++){
            supervisorHandler.registerEmployee(new Supervisor(Integer.toString(j+4),new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(5)));
        }
        directorHandler.registerEmployee(new Director("7", new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(5)));
        for(int i=0;i<4;i++){
            machineHandler.registerEmployee(new Machine(Integer.toString(i),new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(5)));
        }
        operatorHandler.setNextServer(supervisorHandler);
        supervisorHandler.setNextServer(directorHandler);
        directorHandler.setNextServer(machineHandler);
        machineHandler.setNextServer(operatorHandler);
        mainDispatcher.setFirstHandler(operatorHandler);
        executorService=Executors.newFixedThreadPool(10);
    }
    
    @After
    public void tearDown() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            Logger.getLogger(IncomingCallTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void TenIncommingCalls() {
        int servedCalls=0;
        List<Future<Boolean>> callResults=new ArrayList<>();
        for(int i=0;i<10;i++){
            System.out.println("Incomming call "+(i+1));
            callResults.add(executorService.submit(() -> {
                mainDispatcher.dispatchCall();
                return true;
            }));
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(IncomingCallTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        while(callResults.size()>0)
        {
            if(callResults.get(0).isDone()&&!callResults.get(0).isCancelled())
            {
                servedCalls++;
                callResults.remove(0);
            }
        }
        assertEquals(10, servedCalls);
    }
    
    @Test
    public void HundredIncommingCalls() {
        int servedCalls=0;
        List<Future<Boolean>> callResults=new ArrayList<>();
        for(int i=0;i<100;i++){
            System.out.println("Incomming call "+(i+1));
            callResults.add(executorService.submit(() -> {
                mainDispatcher.dispatchCall();
                return true;
            }));
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(IncomingCallTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        while(callResults.size()>0)
        {
            if(callResults.get(0).isDone()&&!callResults.get(0).isCancelled())
            {
                servedCalls++;
                callResults.remove(0);
            }
        }
        assertEquals(100, servedCalls);
    }
}
