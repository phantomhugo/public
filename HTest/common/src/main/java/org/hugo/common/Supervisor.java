/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugo.common;

import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class representing an employee, position supervisor
 * @author Hugo Armando Castellanos Morales
 */
public class Supervisor extends Employee{
    public Supervisor(String id, String name) {
        super(id,name);
    }
    @Override
    public boolean concreteServe() {
        System.out.println("Doing supervisor stuff...");
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(5000, 10000));//Because it is in millis
        } catch (InterruptedException ex) {
            Logger.getLogger(Director.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
}
