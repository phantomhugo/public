/*
 * This file was written by Hugo Armando Castellanos Morales
 */
package org.hugo.common;

import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class which represent the base class Employee, base of all kind of employees
 * @author Hugo Armando Castellanos Morales
 */
public abstract class Employee {
    private boolean busy;
    private String id;
    private String name;
    private Semaphore serveSemaphore;

    public Employee(String id, String name) {
        this.busy = false;
        this.id = id;
        this.name = name;
        serveSemaphore=new Semaphore(1);
    }

    /**
     * Definition of concrete method that must be implemented by the children
     * classes
     * @return True on succeed
     */
    public abstract boolean concreteServe();
    
    /**
     * @return the Employee id number
     */
    public String getId() {
        return id;
    }

    /**
     * @return the employee's Name
     */
    public String getName() {
        return name;
    }
    
    /**
     * @return If the employee is busy
     */
    public boolean isBusy() {
        return busy;
    }

    /**
     * @param busy True to indicate the employee is busy
     */
    public void setBusy(boolean busy) {
        this.busy = busy;
    }
    
    public boolean serve(){
        boolean result=false;
        try {
            serveSemaphore.acquire();
            busy = true;
            StringBuilder sb=new StringBuilder();
            sb.append("Employee ");
            sb.append(this.name);
            sb.append(" with position ");
            sb.append(this.getClass().getName());
            sb.append(" is serving customer ");
            System.out.println(sb.toString());
            concreteServe();
            sb=new StringBuilder();
            sb.append("Employee ");
            sb.append(this.name);
            sb.append(" finished the attention");
            System.out.println(sb.toString());
            result=true;
            busy=false;
            
        } catch (InterruptedException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            serveSemaphore.release();
        }
        return result;
    }
    
    /**
     * Set the employee id number
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Set the employee name
     * @param name the Name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
