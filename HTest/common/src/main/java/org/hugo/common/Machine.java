/*
 * This file was written by Hugo Armando Castellanos Morales
 */
package org.hugo.common;

import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class intended to represent a special kind of "employee", the machine. So it
 * can temporarily serve to customers while a real employee is free
 * @author Hugo Armando Castellanos Morales
 */
public class Machine extends Employee{

    public Machine(String id, String name) {
        super(id, name);
    }
    @Override
    public boolean concreteServe() {
        System.out.println("Doing machine stuff...");
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(5000, 10000));//Because it is in millis
        } catch (InterruptedException ex) {
            Logger.getLogger(Director.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
}
