/*
 * This file was written by Hugo Armando Castellanos Morales
 */
package org.hugo.business;

import org.hugo.common.Employee;

/**
 * This class contains supervisors and in case of an incoming call assign an
 * supervisor or, when all are busy, send the call to the next level
 * @author Hugo Armando Castellanos Morales
 */
public class SupervisorHandler extends Handler{

    public SupervisorHandler() {
        super();
    }

    @Override
    public boolean serve() {
        boolean result=false;
        for(Employee employee:employees){
            if(!employee.isBusy())
            {
                result=employee.serve();
                break;
            }
        }
        if(result==false) {
            System.out.println("Supervisors busy, sending to next level");
        }
        return result;
    }    
}
