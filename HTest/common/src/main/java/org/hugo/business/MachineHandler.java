/*
 * This file was written by Hugo Armando Castellanos Morales
 */
package org.hugo.business;

import org.hugo.common.Employee;

/**
 * Class intended to handle a customer when no employee was available
 * @author Hugo Armando Castellanos Morales
 */
public class MachineHandler extends Handler{
    public MachineHandler() {
        super();
    }
    
    @Override
    public boolean serve() {
        boolean result=false;
        for(Employee employee:employees){
            if(!employee.isBusy())
            {
                //The machine serves but it send the customer to the next
                //responsible, it is the operator
                employee.serve();
                break;
            }
        }
        return result;
    }
}
