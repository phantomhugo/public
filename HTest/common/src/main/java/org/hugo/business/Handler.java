/*
 * This file was written by Hugo Armando Castellanos Morales
 */
package org.hugo.business;

import java.util.ArrayList;
import java.util.List;
import org.hugo.common.Employee;

/**
 *
 * @author Hugo Armando Castellanos Morales
 */
public abstract class Handler {
    private Handler nextServer;
    protected List<Employee> employees;
    
    public Handler(){
        employees = new ArrayList<>();
    }
    /**
     * Return the next handler in the chain
     * @return Next employee handler
     */
    protected Handler getNextServer(){
        return nextServer;
    }
    
    /**
     * Register an employee in the current handler
     * @param currentEmployee Employee to register
     */
    public void registerEmployee(Employee currentEmployee){
        employees.add(currentEmployee);
    }
    
    /**
     * Method intended to be called when a handler must serve a petition
     * @return True if was able to serve
     */
    public abstract boolean serve();
    
    /**
     * Set the next employee handler who can serve the petition
     * @param server Next server
     */
    public void setNextServer(Handler server){
        this.nextServer = server;
    }
}
