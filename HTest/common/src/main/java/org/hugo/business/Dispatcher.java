/*
 * This file was written by Hugo Armando Castellanos Morales
 */
package org.hugo.business;

/**
 * Class intended to handle the customer calls
 * @author Hugo Armando Castellanos Morales
 */
public class Dispatcher {
    Handler firstHandler;
    
    /**
     * Method to send a call to the employees and handle the wait list
     * @return True if someone answered
     */
    public boolean dispatchCall()
    {
        boolean result=false;
        if(firstHandler!=null){
            Handler currentHandler=firstHandler;
            while(!currentHandler.serve()) {
                currentHandler = currentHandler.getNextServer();
            }
            result=true;
        }
        return result;
    }
    
    /**
     * Set the first handler in the chain
     * @param firstHandler The first handler in the chain
     */
    public void setFirstHandler(Handler firstHandler){
        this.firstHandler = firstHandler;
    }
}
