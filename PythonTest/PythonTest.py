import argparse
import os
import xml.etree.ElementTree as ET
import sqlite3
from subprocess import Popen, PIPE

DATABASENAME="Categories.db"

class Node:
    """ Class whic defines a tree node """
    def __init__(self,id,text,extra):
        """ Constructor """
        self.id = id
        self.text = text
        self.extra = extra
        self.children = []

    def AddChild(self,node):
        """ Adds a childs to this node"""
        self.children.append(node)

    def GetChildren(self):
        """ Return al the childrens """
        return self.children

    def GetData(self):
        """ Return a string with the data """
        return "ID: {}<br>Name: {}<br> {}".format(self.id,self.text,self.extra)

class SimpleCondition:
    def __init__(self,column,value):
        self.column = column
        self.value = value

    def GetColumn(self):
        return self.column

    def GetValue(self):
        return self.value

class SqliteDb:
    """ A tiny class to hide a little bit the sqlite database """
    def __init__(self,databaseFileName):
        self.connection = sqlite3.connect(databaseFileName)

    def Commit(self):
        self.connection.commit()
    '''
    Creates the table
    @param tableName Table name
    @param columns Dict with columnName vs Type
    '''
    def CreateTable(self,tableName,columns,constraints):
        finalcolumns = []
        for k,v in columns.items():
            finalcolumnsStr = ""
            finalcolumnsStr += k
            finalcolumnsStr += " "
            finalcolumnsStr += v
            finalcolumns.append(finalcolumnsStr)
        for v in constraints:
            finalcolumns.append(v)
        generalQuery = "CREATE TABLE {} ({})"
        query = generalQuery.format(tableName,",".join(finalcolumns))
        self.currentCursor = self.connection.cursor()
        self.currentCursor.execute(query)

    def Insert(self,table,columnsAndValues):
        columns = []
        values = []
        for k,v in columnsAndValues.items():
            columns.append(k)
            values.append(v)
        self.currentCursor = self.connection.cursor()
        query = "INSERT INTO {} ({}) VALUES({})".format(table,",".join(columns),",".join(values))
        self.currentCursor.execute(query)

    def Select(self,table,columns,condition):
        query = "SELECT {} FROM {} WHERE {}".format(",".join(columns),table,"{}={}".format(condition.GetColumn(),condition.GetValue()))
        self.currentCursor = self.connection.cursor()
        return self.currentCursor.execute(query)

    def SelectIn(self,table,columns,columnIn,dataIn):
        query = "SELECT {} FROM {} WHERE {} IN ({})".format(",".join(columns),table,columnIn,",".join(dataIn))
        self.currentCursor = self.connection.cursor()
        return self.currentCursor.execute(query)

class CategoryLoader:
    """ A tiny class to hide a little bit the sqlite database """
    def __init__(self,xmlString):
        self._internalTree = ET.fromstring(output)

    def CreateTables(self,databaseFileName):
        """ Creates the tables """
        self._DataBase = SqliteDb(databaseFileName)
        categoryColumns = {}
        categoryColumns['id'] = 'TEXT'
        categoryColumns['AutoPayEnabled'] = 'INTEGER'
        categoryColumns['B2BVATEnabled'] = 'INTEGER'
        categoryColumns['BestOfferEnabled'] = 'INTEGER'
        categoryColumns['CategoryLevel'] = 'INTEGER'
        categoryColumns['CategoryName'] = 'TEXT'
        categoryColumns['Expired'] = 'INTEGER'
        categoryColumns['LeafCategory'] = 'INTEGER'
        categoryColumns['LSD'] = 'INTEGER'
        categoryColumns['ORPA'] = 'INTEGER'
        categoryColumns['ORRA'] = 'INTEGER'
        categoryColumns['Virtual'] = 'INTEGER'
        categoryConstraints = ['PRIMARY KEY (id)']
        categoryParentColumns = {'CategoryId':'TEXT','ParentId':'TEXT'}
        categoryParentConstraints = ['PRIMARY KEY (CategoryId,ParentId)']
        self._DataBase.CreateTable("Category",categoryColumns,categoryConstraints)
        self._DataBase.CreateTable("CategoryParent",categoryParentColumns,categoryParentConstraints )

    def LoadInDb(self,databaseFileName):
        """ Loads all categories and write it to the database """
        self.CreateTables(databaseFileName)
        for child in self._internalTree:
            if "CategoryArray" in child.tag:
                self.LoadCategoryArray(child)
        self._DataBase.Commit()

    def LoadCategoryArray(self,node):
        """ Loads all categories in array """
        for child in node:
            if "Category" in child.tag:
                self.LoadCategory(child)

    def LoadCategory(self,node):
        """ Loads the category into the database """
        columnsAndValues = {}
        columnsAndValuesRelation = {}
        for child in node:
            if "AutoPayEnabled" in child.tag is not None:
                columnsAndValues["AutoPayEnabled"] = '1' if child.text is 'true' else '0'
            if "B2BVATEnabled" in child.tag is not None:
                columnsAndValues["B2BVATEnabled"] = '1' if child.text is 'true' else '0'
            if "BestOfferEnabled" in child.tag is not None:
                columnsAndValues["BestOfferEnabled"] = '1' if child.text is 'true' else '0'
            if "CategoryID" in child.tag is not None:
                columnsAndValues["id"] = "'{}'".format(child.text)
                columnsAndValuesRelation["CategoryID"] = "'{}'".format(child.text)
            if "CategoryLevel" in child.tag is not None:
                columnsAndValues["CategoryLevel"] = child.text
            if "CategoryName" in child.tag is not None:
                columnsAndValues["CategoryName"] = "{}{}{}".format("'",child.text,"'")
            if "CategoryParentID" in child.tag is not None:
                #columnsAndValues["CategoryParentID"] = child.text
                columnsAndValuesRelation["ParentID"] = "'{}'".format(child.text)
            if "Expired" in child.tag is not None:
                columnsAndValues["Expired"] = '1' if child.text is 'true' else '0'
            if "LeafCategory" in child.tag is not None:
                columnsAndValues["LeafCategory"] = '1' if child.text is 'true' else '0'
            if "LSD" in child.tag is not None:
                columnsAndValues["LSD"] = '1' if child.text is 'true' else '0'
            if "ORPA" in child.tag is not None:
                columnsAndValues["ORPA"] = '1' if child.text is 'true' else '0'
            if "ORRA" in child.tag is not None:
                columnsAndValues["ORRA"] = '1' if child.text is 'true' else '0'
            if "Virtual" in child.tag is not None:
                columnsAndValues["Virtual"] = '1' if child.text is 'true' else '0'
        self._DataBase.Insert("Category",columnsAndValues)
        if len(columnsAndValuesRelation) == 2 and columnsAndValuesRelation["ParentID"] != columnsAndValuesRelation["CategoryID"]:
            self._DataBase.Insert("CategoryParent",columnsAndValuesRelation)

def FillTree(childMap,categoryToRender):
    """ Fill the tree with data from the database """
    children = []
    condition=SimpleCondition("id","'{}'".format(categoryToRender))
    for elem in db.Select("Category",["id","CategoryName","CategoryLevel","BestOfferEnabled"],condition):
        childMap[categoryToRender] = Node(elem[0],elem[1],"Level: {}<br>BestOffer: {}".format(elem[2],elem[3]))
    condition=SimpleCondition("ParentId","'{}'".format(categoryToRender))
    for elem in db.Select("CategoryParent",["CategoryId"],condition):
        children.append(elem[0])
    for elem in db.SelectIn("Category",["id","CategoryName","CategoryLevel","BestOfferEnabled"],"id",children):
        #Add to tree
        node = Node(elem[0],elem[1],"Level: {}<br>BestOffer: {}".format(elem[2],elem[3]))
        childMap[categoryToRender].AddChild(node)
        childMap[elem[0]] = node
    for elem in children:
        FillTree(childMap,elem)

def RenderHtml(childMap,categoryToRender,styleClass):
    """ Render the HTML """
    childrenStr=""
    renderedPortion=""
    if categoryToRender in childMap:
        for elem in childMap[categoryToRender].GetChildren():
            childrenStr+=RenderHtml(childMap,elem.id,"")
        renderedPortion="<ul {}><li><span>{}</span>{}</li></ul>".format(styleClass,childMap[categoryToRender].GetData(),childrenStr)
    return renderedPortion

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--rebuild", help="Creates a SQLite database from the categories API (deletes previous versions)", action="store_true")
    parser.add_argument("--render", help="Generates an HTML file which show the category", type=str)
    args = parser.parse_args()
    if args.rebuild:
        print("Rebuilding")
        if os.path.isfile(DATABASENAME):
            os.remove(DATABASENAME)
        p = Popen(["sh","get_ebay_categories.sh"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, err = p.communicate(None)
        with open("example.xml", "w") as text_file:
            text_file.write(output.decode("utf-8"))
        loader = CategoryLoader(output.decode("utf-8"))
        loader.LoadInDb(DATABASENAME)
    else:
        categoryToRender=args.render
        db = SqliteDb(DATABASENAME)
        condition=SimpleCondition("id","'{}'".format(categoryToRender))
        data = db.Select("Category",["COUNT(id)"],condition).fetchone()[0]
        if data == 0:
            print("No category with ID: {}".format(categoryToRender))
        else:
            childMap = {}
            FillTree(childMap,categoryToRender)
            finalHtml='<html><head><title>Category tree</title><link rel="stylesheet" type="text/css" href="tree.css"></head><body><div id="wrapper">'
            finalHtml+=RenderHtml(childMap,categoryToRender,'class="tree"')
            finalHtml+='<div class="clearer"></div></div></body></html>'
            with open("{}.html".format(categoryToRender), "w") as text_file:
                text_file.write(finalHtml)
